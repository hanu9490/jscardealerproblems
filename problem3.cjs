function problem3(inventory) {
  // extract all the car models from the inventory
  let carModels = [];
  for (let i = 0; i < inventory.length; i++) {
    carModels.push(inventory[i].car_model);
  }
  
  // sort the car models in alphabetical order
  carModels.sort();

  return carModels;
}

module.exports = problem3;
