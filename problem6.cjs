function problem6(inventory) {
  const bmwAndAudi = [];

  for (let i = 0; i < inventory.length; i++) {
    const car = inventory[i];

    if (car.car_make === "BMW" || car.car_make === "Audi") {
      bmwAndAudi.push(car);
    }
  }

  return bmwAndAudi;
}


module.exports = problem6;
