function problem2(inventory) {
  let lastCar = inventory[inventory.length - 1];
  let make = lastCar.car_make;
  let model = lastCar.car_model;

  return [make, model];
}

module.exports = problem2;
