function problem4(data) {
  let years = [];
  for (let i = 0; i < data.length; i++) {
    years.push(data[i].car_year);
  }
  return years;
}

module.exports = problem4;
