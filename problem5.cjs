function problem5(inventory) {
  let olderCars = [];

  for (let i = 0; i < inventory.length; i++) {
    let carYear = inventory[i].car_year;
    if (carYear < 2000) {
      olderCars.push(inventory[i]);
    }
  }

  

  return olderCars;
}

module.exports = problem5;
