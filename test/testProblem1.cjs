const problem1 = require("../problem1.cjs");
let inventory = require("../inventoryList.cjs");

const car33 = problem1(inventory, 33);

// console.log(car33);

console.log(`Car 33 is a ${car33.car_year} ${car33.car_make} ${car33.car_model}`);
