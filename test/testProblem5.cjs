let problem5 = require("../problem5.cjs");
let inventory = require("../inventoryList.cjs");

let olderCars = problem5(inventory);
console.log(`There are ${olderCars.length} cars older than the year 2000.`);
console.log(olderCars);
